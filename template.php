<?php
function arbit_preprocess_page(&$vars) {
    if($vars['logged_in']) {
    $vars['site_slogan'] = t('You are logged in, @username', array('@username' => $vars['user']->name));
  }
  else {
    $vars['site_slogan'] = t('');
  }
}